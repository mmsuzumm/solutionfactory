import requests
import json
from .models import MailingList, Clients


def send_post_to_server(message_id: str or int, json_obj: dict) -> tuple[int, dict]:
    headers = {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
                                '.eyJleHAiOjE3MDE4Njk2ODcsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Im1hdHZTdSJ9'
                                '.exBpSZqAlOsMaI3fpMS6RK41fHxZbaKabXm1VIwm57Y'}
    url = 'https://probe.fbrq.cloud/v1/send'
    if type(message_id) == int:
        message_id = str(message_id)
    elif type(message_id) == str:
        try:
            message_id = str(int(message_id))
        except ValueError:
            return 'Uncorrected message id', ValueError
    else:
        raise 'You entered uncorrected id datatype. Check it and try again'

    full_url = url + '/' + message_id
    post_json = json.loads(json.dumps(json_obj, indent=4))
    headers = json.loads(json.dumps(headers, indent=4))
    response = requests.post(url=full_url, headers=headers, json=post_json)
    if response.status_code == 200:
        return response.status_code, response.json()
    else:
        return response.status_code


def get_data_from_db():
    mailing_list_now = MailingList.objects.values()
    data = [i for i in mailing_list_now]
    for i in data:
        phone_by_code = get_numbers_and_utc(Clients.objects.filter(operator_code=i['phone_code']))
        phone_by_tag = get_numbers_and_utc(Clients.objects.filter(tag=i['tags']), phone_by_code)
        all_phone_numbers = phone_by_tag


def get_numbers_and_utc(data_base_queryset, phone_numbers_to_send=[]):
    for clients in data_base_queryset:
        phone_number = str(clients.phone_number)
        timezone = int(clients.timezone)
        if [phone_number, timezone] not in phone_numbers_to_send:
            phone_numbers_to_send.append([phone_number, timezone])
    return phone_numbers_to_send


if __name__ == '__main__':
    test_dict_to_send = {"id": 1,
                         "phone": 79859772386,
                         "text": "Test message"}
    print(send_post_to_server(
        url='https://probe.fbrq.cloud/v1/send',
        message_id=1,
        json_obj=test_dict_to_send
    ))
