from django.urls import path
from .views import *
urlpatterns = [
    path("index/", index, name='index'),
    path("add_client/", add_client, name='add_client'),
    path("clients_page/", clients_page, name='clients_page'),
    path("update_client/<int:pk>/", update_client, name='update_client'),
    path("delete_client/<int:pk>/", delete_client, name='delete_client'),
    path("create_mailing_list/", create_mailing_list, name='create_mailing_list'),
]


