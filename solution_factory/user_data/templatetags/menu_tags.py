from django import template

register = template.Library()


@register.simple_tag()
def nav_bar_tags():
    menu = [
        {'title': 'Home page', 'url_name': 'index'},
        {'title': 'Add client', 'url_name': 'add_client'},
        {'title': 'About clients', 'url_name': 'clients_page'},
        {'title': 'Send Messages', 'url_name': 'create_mailing_list'},
    ]

    return menu
