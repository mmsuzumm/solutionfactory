from django import forms
from .models import *


class AddClientForm(forms.ModelForm):
    class Meta:
        model = Clients
        fields = ['phone_number', 'operator_code', 'tag', 'timezone']
        widgets = {
            'tag': forms.TextInput(attrs={'cols': '10', 'rows': '1'})
        }


class MyDataTimeInput(forms.DateTimeInput):
    format_key = 'DATETIME_INPUT_FORMATS'
    input_type = 'datetime-local'


class MailingListForm(forms.ModelForm):
    class Meta:
        model = MailingList
        fields = ['start_mailing', 'end_mailing', 'phone_code', 'tags', 'message_text', ]
        widgets = {
            'start_mailing': MyDataTimeInput(),
            'end_mailing': MyDataTimeInput(),
            'phone_code': forms.TextInput(attrs={'cols': '10', 'rows': '1'}),
            'tags': forms.TextInput(attrs={'cols': '10', 'rows': '1'})
        }

