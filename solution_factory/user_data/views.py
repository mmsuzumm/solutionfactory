from django.shortcuts import render, redirect

from .forms import *
from .models import Clients
from .utils import get_data_from_db


def index(request):
    return render(request, 'user_data/index.html', )


def add_client(request):
    if request.method == 'POST':
        form = AddClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('add_client')
    else:
        form = AddClientForm()
    context = {'form': form, }
    return render(request, 'user_data/add_client.html', context=context)


def clients_page(request):
    get_data_from_db()
    clients = Clients.objects.all()
    context = {'clients': clients, }
    return render(request, 'user_data/clients_page.html', context=context)


def update_client(request, pk):
    client = Clients.objects.get(id=pk)
    if request.method == 'POST':
        form = AddClientForm(request.POST, instance=client)
        if form.is_valid():
            form.save()
            return redirect('clients_page')
    else:
        form = AddClientForm(instance=client)
    context = {'form': form,
               'pk': pk}
    return render(request, 'user_data/update_client.html', context=context)


def delete_client(request, pk):
    client = Clients.objects.get(id=pk)
    client.delete()
    return redirect('clients_page')


def create_mailing_list(request):
    if request.method == 'POST':
        form = MailingListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('create_mailing_list')
    else:
        form = MailingListForm()
    context = {'form': form, }
    return render(request, 'user_data/create_mailing_list.html', context=context)

