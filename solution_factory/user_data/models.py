from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField


class Clients(models.Model):
    phone_number = PhoneNumberField(unique=True)
    operator_code = models.SmallIntegerField(blank=False)
    tag = models.TextField()
    timezone = models.SmallIntegerField()

    def __str__(self):
        return str(self.phone_number)

    def get_absolute_url(self):
        return reverse('update_client', kwargs={'client_id': self.pk})


class MailingList(models.Model):
    start_mailing = models.DateTimeField()
    message_text = models.TextField()
    phone_code = models.TextField()
    tags = models.TextField()
    end_mailing = models.DateTimeField()

    def __str__(self):
        return f'Was sent {self.message_text} at {self.start_mailing} to clients with tags {self.tags} and phone code' \
               f'{self.phone_code}'


class Messages(models.Model):
    sent_date = models.CharField(max_length=11, blank=False)
    operator_code = models.DateTimeField(blank=False)
    is_sent = models.BooleanField()
    group_sent_id = models.ForeignKey('MailingList', on_delete=models.DO_NOTHING)
    client_id = models.ForeignKey('Clients', on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'Sent to {self.client_id}'


